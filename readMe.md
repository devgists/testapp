# testApp

A meteor app that allows users to store private notes and images. The app makes use of:

  - Google Authentication
  - UploadCare (https://uploadcare.com/) for image storage
  - MongoDB

# App Screenshots!
signup
[![N|Solid](http://michaelkyazze.com/meteor/meteor-1.png)](https://nodesource.com/products/nsolid)

authentication
[![N|Solid](http://michaelkyazze.com/meteor/meteor-2.png)](https://nodesource.com/products/nsolid)

dashboard
[![N|Solid](http://michaelkyazze.com/meteor/meteor-3.png)](https://nodesource.com/products/nsolid)

image upload
[![N|Solid](http://michaelkyazze.com/meteor/meteor-4.png)](https://nodesource.com/products/nsolid)

private uploads
[![N|Solid](http://michaelkyazze.com/meteor/meteor-5.png)](https://nodesource.com/products/nsolid)



