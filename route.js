/**
 * Created by s212226738 on 2017/05/27.
 */
Router.route('/', function () {
    this.render('index');
    //this.render('firstPage');
});

Router.route('/dashboard', function () {
    this.render('dashboardPage');
});

Router.route('/index', function () {
    this.render('index');
});