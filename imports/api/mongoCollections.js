/**
 * Created by s212226738 on 2017/05/28.
 */
import {Mongo} from 'meteor/mongo';

export const MyNotes = new Mongo.Collection('notes');
export const MyImages = new Mongo.Collection('myimages');