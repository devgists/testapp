import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Meteor } from 'meteor/meteor'
import {MyNotes} from '../imports/api/mongoCollections.js';
import {MyImages} from '../imports/api/mongoCollections.js';
import uploadcare from 'meteor/uploadcare:uploadcare-widget'

import './main.html';

/**
 * Redirect to dashboard if user is already logged on
 */
if(Meteor.userId())
{
    Router.go('dashboard', {name: '/dashboardPage'});
}

Template.firstPage.helpers({
    tagLine: 'meteor test app'
});

Template.dashboardPage.helpers({
    /**
     * Query mongo for a given user's notes
     */
    notes() {
        return MyNotes.find({owner: Meteor.user().services.google.email});
    },
    /**
     * Query mongo for a given user's images
     */
    images() {
        return MyImages.find({owner: Meteor.user().services.google.email});
    },
    /**
     * Query mongo active user object
     */
    users() {
        return Meteor.user();
    },
    /**
     * Image unique identifier: The images are stored using: https://uploadcare.com/
     */
    uuid() {
        return Template.instance().uuid.get()
    },
    /**
     * Unique image URL as returned by the: https://uploadcare.com/ API
     */
    cdnUrl() {
        return Template.instance().cdnUrl.get()
    }
});


Template.dashboardPage.onRendered(function() {
    let widget = uploadcare.Widget('#file-show')
    widget.onUploadComplete(info => {
        // Handle uploaded file info.
        this.uuid.set(info.uuid)
        this.cdnUrl.set(info.cdnUrl)
        MyImages.insert({owner: Meteor.user().services.google.email, imgURL: info.cdnUrl});
    })
})

Template.dashboardPage.onCreated(function() {
    this.uuid = new ReactiveVar('')
    this.cdnUrl = new ReactiveVar('')
})


Template.dashboardPage.events({
    'submit .new-note'(event) {
        var note = event.target.newNote.value;
        var email = event.target.emailAddress.value;
        MyNotes.insert({owner: email, name: note});
    }
});